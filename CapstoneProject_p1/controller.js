let total = 0

// Hàm đếm tổng số lượng sản phẩm trong giỏ hàng
let countNum = (data) => {
  let count = 0;
  for (let index = 0; index < data.length; index++) {
    count += data[index].count;
  }
  return count;
};
// mở giỏ hàng
export let openTable = (selector) => {
  let element = document.querySelector(selector);

  element.classList = "cart_table animate__animated animate__fadeInRight";

  element.style.display = "block";
  element.style.right = "0%";
};
// đóng giỏ hàng
export let cancelTable = (selector) => {
  let element = document.querySelector(selector);

  element.classList = "cart_table animate__animated animate__fadeOutRight";
  setTimeout(() => {
    element.style.display = "none";
    element.style.right = "-100%";
  }, 500);
};
// render danh sách sản phẩm
export let renderList = (data) => {
  let contentDivList = "";
  for (let index = 0; index < data.length; index++) {
    let item = data[index];
    let contentDivItem = `
      <div class="shop_item col-3 py-1 px-2">
        <div class="card" style="width: 100%">
          <img class="card-img-top pt-2" src=${item.img} alt="Card image cap" />
          <div class="card-body">
            <h5 class="card-title">${item.name}</h5>
            <div class="card-text" style="line-height:1.3em">
              <p>
                ${item.screen} <br>
                ${item.backCamera}<br>
                ${item.frontCamera}<br>
                ${item.desc}<br>
              </p>
            </div>
            <div class='d-flex justify-content-between'>
              <span class='font-weight-bold text-danger'> $${item.price}</span>
              <button onclick='addToCart(${item.id})' class="btn btn-primary">Add</button>
            </div>
          </div>
        </div>
      </div>
    `;
    contentDivList += contentDivItem;
  }
  // gọi dữ liệu lưu trong local để đếm số lượng sản phẩm
  let dataJson = localStorage.getItem("ProductList");
  let cartArr = JSON.parse(dataJson);
  let countSum = countNum(cartArr);
  document.querySelector("#shop_binding").innerHTML = contentDivList;
  document.querySelector("#cartNumber").innerHTML = countSum;
};
// render giỏ hàng
export let renderTable = (data) => {
  let content = "";
  total = 0
  for (let index = 0; index < data.length; index++) {
    let item = data[index];
    let contenTr = `
    <div class='d-flex justify-content-around'>
              <div class='col-2'>
                  <img src='${item.img}' class='w-50'></img>
              </div>
              <p class='col-2'>${item.name}</p>
              <p class='col-2'>
                <button onclick='upDownCount(${
                  item.id
                },${false})' class='btn btn-light'>
                  <i class="fa fa-angle-left"></i>
                </button>
                ${item.count}
                <button onclick='upDownCount(${
                  item.id
                },${true})' class='btn btn-light'>
                  <i class="fa fa-angle-right"></i>
                </button>
              </p>
              <p class='col-2'>$${item.price * item.count}</p>
              <div class='col-2'>
                <button onclick='removeToCart(${
                  item.id
                })' class='btn btn-danger'><i class="fa fa-trash"></i></button>
              </div>
    </div>
              <hr class='border-danger w-100'>
    `;
    content += contenTr;
    total += item.price * item.count;
  }
  document.querySelector("#cart_content").innerHTML = content;
  document.querySelector(
    "#total"
  ).innerHTML = `Total: $${total.toLocaleString()}`;
};

export let renderPurchase = (data) => {
  cancelTable(".cart_table");

  let content = "";
  if (data.length == 0) {
    document.querySelector('#btn_dialog2').disabled = true
    content = "Nothing in cart";
  } else {
    document.querySelector('#btn_dialog2').disabled = false
    for (let index = 0; index < data.length; index++) {
      let item = data[index];
      let itemContent = `
      <div class='d-flex justify-content-between'>
      <p>${item.count} x ${item.name}</p>
      <p>$${item.price * item.count}</p>
      </div>
      `;
      content += itemContent;
    }
  }
  document.querySelector("#pay_dialog").innerHTML = content;
  document.querySelector("#total_dialog").innerHTML = `$${total.toLocaleString()}`;
};

export let openDialog2 = () => { 
  let content = `You can pay $${total.toLocaleString()} by card or any online transaction method after the products have been dilivered to you`
  document.querySelector('#order_success').innerHTML = content
 }

