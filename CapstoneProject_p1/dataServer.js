const urlBase = "https://62b9758941bf319d227cec4b.mockapi.io/";

export let dataServer = {
  callDataServer: () => {
    return axios({
      url: urlBase + "capstoneJs",
      method: "GET",
    });
  },
};
