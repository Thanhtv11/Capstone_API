const BASE_URL = `https://62b9758941bf319d227cec4b.mockapi.io/capstoneJs`

export let apiService = {
    GetData: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        })
    },
    AddData: (item) => {
        return axios({
            url:BASE_URL,
            method: "POST",
            data: item,
        })
    },

    DeleteData: (id) => {
        return axios({
            url: `https://62b9758941bf319d227cec4b.mockapi.io/capstoneJs/${id}`,
            method: "DELETE",
            data: id,
        })
    },

    GetIdData: (id) => {
        return axios({
            url: `https://62b9758941bf319d227cec4b.mockapi.io/capstoneJs/${id}`,
            method: "GET",
            data: id,
        })

    },

    UpdateData: (obj) =>{
        return axios({
            url: `https://62b9758941bf319d227cec4b.mockapi.io/capstoneJs/${obj.id}`,
            method: "PUT",
            data: obj,
        })

    }
}