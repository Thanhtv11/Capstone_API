 export let inputController = {
    getValue: () => {
        let tenSP = document.getElementById("nameItem").value;
        let giaSP = document.getElementById("priceItem").value;
        let hinhanhSp = document.getElementById("imageItem").value;
        let moTaSP = document.getElementById("descItem").value;
        let objItem =  {
            name:tenSP,
            price:giaSP,
            img:hinhanhSp,
            desc:moTaSP,
        };
        return objItem;
    },

    hienInputRong: () =>{
        document.getElementById("nameItem").value = '';
        document.getElementById("priceItem").value = '';
        document.getElementById("imageItem").value = '';
        document.getElementById("descItem").value = '';
    }
}

