let arrItem = [];

let addNewItem = () => {
  let objItem = inputController.getValue();
  let regex = Validation.regexNumber(objItem.price, "#errormessage2");
  let valid =
    Validation.validateRong(objItem.name, "#errormessage1") &
    Validation.validateRong(objItem.price, "#errormessage2") &
    Validation.validateRong(objItem.img, "#errormessage3") &
    Validation.validateRong(objItem.desc, "#errormessage4") &
    regex;
  if (!valid) {
    if (!regex && objItem.price !== "") {
      document.querySelector(
        "#errormessage2"
      ).innerHTML = `Must contains ONLY numbers`;
    }
    return;
  }
  arrItem.push(objItem);
  apiService
    .AddData(objItem)
    .then((res) => {
      renderDanhSachService();
      inputController.hienInputRong();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.addNewItem = addNewItem;

let deleteItem = (id) => {
  apiService
    .DeleteData(id)
    .then((res) => {
      let filterspan = document.querySelector("#errormessage2");
      if (filterspan){
        filterspan.innerHTML = '';
      }
      inputController.hienInputRong();
      renderDanhSachService();
      window.scroll(0, 0);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteItem = deleteItem;

let updateItem = () => {
  let id = document.querySelector("#hiddeninput").value;
  if (!id) {
    alert("Please click the 'EDIT' button of the item you want to edit");
    return;
  }
  let inputValue = inputController.getValue();
  let regex = Validation.regexNumber(inputValue.price);
  let valid =
    Validation.validateRong(inputValue.name, "#errormessage1") &
    Validation.validateRong(inputValue.price, "#errormessage2") &
    Validation.validateRong(inputValue.img, "#errormessage3") &
    Validation.validateRong(inputValue.desc, "#errormessage4") &
    regex;
  if (!valid) {
    if (!regex && inputValue.price !== "") {
      document.querySelector(
        "#errormessage2"
      ).innerHTML = `Must contains ONLY numbers`;
    }
    return;
  }
  inputValue = { ...inputValue, id };
  apiService
    .UpdateData(inputValue)
    .then((res) => {
      document.querySelector("#hiddeninput").value = "";
      document.querySelector("#errormessage2").innerHTML = "";
      renderDanhSachService();
      inputController.hienInputRong();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.updateItem = updateItem;

let getIdData = (id) => {
  document.querySelector("#hiddeninput").value = id;
  document.querySelector("#errormessage1").innerHTML = "";
  document.querySelector("#errormessage2").innerHTML = "";
  document.querySelector("#errormessage3").innerHTML = "";
  document.querySelector("#errormessage4").innerHTML = "";
  apiService
    .GetIdData(id)
    .then((res) => {
      document.querySelector("#nameItem").value = res.data.name;
      document.querySelector("#priceItem").value = res.data.price;
      document.querySelector("#imageItem").value = res.data.img;
      document.querySelector("#descItem").value = res.data.desc;
      window.scroll(0, 0);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.getIdData = getIdData;

let filterItem = () => {
  let value = document.querySelector(".nhapInput").value;
  apiService
    .GetData()
    .then((res) => {
      arrItem = res.data;
      let findItem = arrItem.filter((item) =>
        item.name.toLowerCase().includes(value.toLowerCase())
      );
      if (findItem) {
        renderTable(findItem);
        return;
      }
      renderTable(arrItem);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.filterItem = filterItem;

let renderTable = (list) => {
  let contentHTML = "";
  for (let i = 0; i < list.length; i++) {
    let data = list[i];
    let contentLayout = `<tr>
                          <td>${data.id}</td>
                          <td>${data.name}</td>
                          <td>${data.price}</td>
                          <td>${data.img}</td>
                          <td>${data.desc}</td>
                          <td><button onclick="deleteItem(${data.id})" class="btn btn-primary">Xóa</button>
                          <button onclick="getIdData(${data.id})" class="btn btn-success">Sửa</button>
                          </td>
                        </tr>`;
    contentHTML += contentLayout;
  }
  document.getElementById("tbody_content").innerHTML = contentHTML;
};

let renderDanhSachService = () => {
  apiService
    .GetData()
    .then((res) => {
      arrItem = res.data;
      renderTable(arrItem);
    })
    .catch((err) => {
      console.log(err);
    });
};

apiService
  .GetData()
  .then((res) => {
    arrItem = res.data;
    renderTable(arrItem);
  })
  .catch((err) => {
    console.log(err);
  });

let reloading = () => {
  inputController.hienInputRong();
  document.querySelector("#hiddeninput").value = "";
  document.querySelector(".nhapInput").value = "";
};

window.onload = reloading;
import { inputController } from "./InputController.js";
import { apiService } from "./DataFromAPI.js";
import { Validation } from "./Validation.js";
