export let Validation = {
  validateRong: (value, error) => {
    if (value === "") {
      document.querySelector(`${error}`).innerHTML = `Please enter information`;
      return false;
    }
    document.querySelector(`${error}`).innerHTML = "";
    return true;
  },

  regexNumber: (input) => {
    if (/([0-9])/.test(input) ) {
      return true;
    }
    return false;
  },
};
